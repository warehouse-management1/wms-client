# wms-client

## Project setup
Project uses yarn for builds and dependency management

```
# use direnv so I can call 3rd party node.js based tools without having to prepend npx to everything.  Also allows for putting all the dependencies the project requires in one place.
direnv allow
npm install yarn
yarn 
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
