module.exports = {
  transpileDependencies: ['vuetify'],
  devServer: {
    port: 3000,
    proxy: {
      '/api': {
        target: 'http://localhost:8080',
        ws: false,
      },
    },
    historyApiFallback: {
      verbose: true,
      index: '/index.html',
      rewrites: [{ from: '/login.php', to: '/login.html' }],
    },
  },
  css: {
    requireModuleExtension: true,
  },
  configureWebpack: {
    devtool: 'source-map',
    externals: {
      moment: 'moment',
    },
  },
};
