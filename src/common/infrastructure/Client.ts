import axios, { AxiosInstance, AxiosPromise, AxiosResponse } from 'axios';
import Denque from 'denque';
import { DateTime } from 'luxon';
import { isIdentifiable, isNotJustIdentifiable } from '@/domain/Identifiable';
import { isDomainType } from '@/domain/DomainType';
import Page, { emptyPage, isPage, nextPage } from '@/domain/Page';
import RequestError from '@/domain/RequestError';
import { Store } from 'vuex';
import PageRequest, { PageRequestConstructor } from '@/domain/PageRequest';
import RootState from '@/vuex-store/RootState';
import _assign from 'lodash/assign';

const dateFormat = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d+Z$/;

const handleOnFullFilled = (response: AxiosResponse): MiddlewareResponse<any> => {
  return new MiddlewareResponse(response.status, response.data);
};

const responseTransformer = (data: any): any => {
  if (data !== null) {
    const stack = new Denque();

    if (isPage(data)) {
      data.elements.forEach((value) => stack.push(value));
    } else {
      stack.push(data);
    }

    while (!stack.isEmpty()) {
      const el = stack.pop();

      for (const key in el) {
        // apparently it is safer to call hasOwnProperty this way
        if (Object.prototype.hasOwnProperty.call(el, key)) {
          const value = el[key];

          if (typeof value === 'string' && dateFormat.test(value)) {
            el[key] = DateTime.fromISO(value, { zone: 'utc' }).toLocal();
          } else if (typeof value === 'object') {
            stack.push(value);
          }
        }
      }
    }
  }

  return data;
};

const isPrimitive = (value: any): boolean => {
  switch (typeof value) {
    case 'boolean':
    case 'number':
    case 'bigint':
    case 'string':
      return true;
    default:
      return false;
  }
};

const sanitizePostPutBody = (body: any): any => {
  const result: { [key: string]: any } = {};

  if (body !== null) {
    // only checking very shallow with the top level properties, may need to enhance.  Don't want to deal with arrays
    // since the api shouldn't allow bulk updates as a rule anyway.
    for (const key in body) {
      if (Object.prototype.hasOwnProperty.call(body, key) && body[key] !== null) {
        if (
          isPrimitive(body[key]) ||
          (isIdentifiable(body[key]) && body[key].id !== null) ||
          isNotJustIdentifiable(body[key]) ||
          isDomainType(body[key])
        ) {
          result[key] = body[key];
        }
      }
    }
  }

  return result;
};

const paramSerializer = (params: any): string => {
  return Object.keys(params)
    .map((key: string) => {
      const paramValue = params[key];

      if (paramValue === undefined) {
        return '';
      } else if (Array.isArray(paramValue)) {
        return paramValue
          .map((element: any) => {
            return `${key}=${element}`;
          })
          .join('&');
      } else if (paramValue instanceof DateTime) {
        return `${key}=${paramValue.toUTC()}`;
      } else {
        return `${key}=${paramValue}`;
      }
    })
    .join('&');
};

export default class Client {
  private accessToken: string;
  private store: Store<RootState>;
  private ax: AxiosInstance;
  private defaultHttpHeaders: Record<string, string> = {
    accept: 'application/json',
    contentType: 'application/json',
  };

  constructor(
    accessToken: string,
    store: Store<RootState>,
    beforeRequest: () => void = () => {
      return;
    },
    afterRequest: () => void = () => {
      return;
    },
  ) {
    this.accessToken = accessToken;
    this.store = store;
    this.beforeRequest = beforeRequest;
    this.afterRequest = afterRequest;
    this.ax = axios.create({
      baseURL: '/api',
      transformResponse: responseTransformer,
      responseType: 'json',
    });
  }

  public get(
    path: string,
    pageRequest?: PageRequest,
    requestOptions?: Record<string, string>,
  ): Promise<MiddlewareResponse<any>> {
    this.beforeRequest();

    return this.ax
      .get(path, {
        params: { ...pageRequest },
        paramsSerializer: paramSerializer,
        headers: this.buildHeaders(requestOptions),
      })
      .then(handleOnFullFilled)
      .then((r: any) => {
        this.afterRequest();
        return r;
      })
      .catch((r: any) => {
        this.afterRequest();
        return this.handleOnRejected(r);
      });
  }

  public async getAllPages<T>(
    path: string,
    values: T[],
    compareFn?: (a: T, b: T) => number,
    pageInRequest?: PageRequest,
  ) {
    let pageRequest = pageInRequest ? pageInRequest : new PageRequestConstructor(1, 100);
    let page = emptyPage;

    values.splice(0, values.length); // reset list

    // loop until done loading all
    do {
      const response = await this.get(path, pageRequest);

      if (this.doesNotHaveErrors(response)) {
        page = response.body as Page<any>;

        if (page !== null) {
          values.push(...page.elements);
          pageRequest = nextPage(page);
        }
      } else {
        page = emptyPage;
      }
    } while (page !== null && !page.last);

    if (compareFn !== null) {
      values.sort(compareFn);
    }
  }

  public getPdf(path: string): AxiosPromise {
    this.beforeRequest();
    const requestOptions: Record<string, string> = {
      accept: 'application/pdf',
    };

    return axios
      .get(`/api${path}`, {
        responseType: 'blob',
        headers: this.buildHeaders(requestOptions),
      })
      .then((r: any) => {
        this.afterRequest();
        return r;
      })
      .catch((r: any) => {
        this.afterRequest();
        return this.handleOnRejected(r);
      });
  }

  public post(
    path: string,
    body: any,
    sanitizeBody = false,
    requestOptions?: Record<string, string>,
  ): Promise<MiddlewareResponse<any>> {
    this.beforeRequest();

    const requestBody = sanitizeBody ? sanitizePostPutBody(body) : body;

    return this.ax
      .post(path, body, { headers: this.buildHeaders(requestOptions) })
      .then(handleOnFullFilled)
      .then((r: any) => {
        this.afterRequest();
        return r;
      })
      .catch((r: any) => {
        this.afterRequest();
        return this.handleOnRejected(r);
      });
  }

  public put(
    path: string,
    body: any,
    sanitizeBody = false,
    requestOptions?: Record<string, string>,
  ): Promise<MiddlewareResponse<any>> {
    this.beforeRequest();

    const requestBody = sanitizeBody ? sanitizePostPutBody(body) : body;

    return this.ax
      .put(path, body, { headers: this.buildHeaders(requestOptions) })
      .then(handleOnFullFilled)
      .then((r: any) => {
        this.afterRequest();
        return r;
      })
      .catch((r: any) => {
        this.afterRequest();
        return this.handleOnRejected(r);
      });
  }

  public delete(
    path: string,
    pageRequest?: PageRequest,
    requestOptions?: Record<string, string>,
  ): Promise<MiddlewareResponse<any>> {
    this.beforeRequest();

    return this.ax
      .delete(path, {
        params: { ...pageRequest },
        paramsSerializer: paramSerializer,
        headers: this.buildHeaders(requestOptions),
      })
      .then(handleOnFullFilled)
      .then((r: any) => {
        this.afterRequest();
        return r;
      })
      .catch((r: any) => {
        this.afterRequest();
        return this.handleOnRejected(r);
      });
  }

  private buildHeaders = (options?: Record<string, string>) =>
    _assign({}, this.defaultHttpHeaders, { Authorization: `Bearer ${this.getAccessToken()}` }, options);

  private handleOnRejected(reason: any): MiddlewareResponse<any> {
    const response: AxiosResponse = reason.response; // TODO need to figure out why this works without the compiler complaining

    switch (response.status) {
      case 401:
        return new MiddlewareResponse<any>(response.status);
      case 204:
      case 403:
      case 400: // middleware was unable to process the request probably due to an invalid POST or PUT body
      case 404: // client asked for something that did not exist.  Also can occur when a page that is beyond existing data available to the server returns empty results
      case 409: // handle conflict request.
      case 500: // something has gone horribly wrong server side
      case 501: // client asked for an endpoint that exists, but has not been finished yet
        return new MiddlewareResponse(response.status, null, response.data);
      default:
        return new MiddlewareResponse(response.status, null, [new RequestError('Unexpected Error')]);
    }
  }

  private getAccessToken(): string {
    if (this.accessToken == null) {
      this.accessToken = localStorage.getItem('accessToken')!;
    }

    return this.accessToken;
  }

  private readonly beforeRequest: () => void = () => {
    return;
  };

  private readonly afterRequest: () => void = () => {
    return;
  };

  private doesNotHaveErrors(response: MiddlewareResponse<any>): boolean {
    if (response.errors.length > 0) {
      this.store.commit('addRequestErrors', response.errors);
      return false;
    } else {
      return true;
    }
  }
}

export class MiddlewareResponse<T> {
  public readonly status: number;
  public readonly body?: T | null;
  public readonly errors: RequestError[];

  constructor(status: number, body?: T, errors: RequestError[] = []) {
    this.status = status;
    this.body = body;
    this.errors = errors;
  }
}
