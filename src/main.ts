import Vue from 'vue';
import App from '@/App.vue';
import router from '@/router';
import vuetify from '@/plugins/vuetify';
import ClientPlugin, { ClientPluginOptions } from '@/plugins/ClientPlugin';
import { createStore } from '@/vuex-store';

const accessToken = localStorage.getItem('accessToken');

if (accessToken) {
  const store = createStore(accessToken);

  Vue.config.productionTip = false;
  Vue.use(ClientPlugin, { accessToken, store } as ClientPluginOptions);

  new Vue({
    router,
    store,
    vuetify,
    render: (h) => h(App),
  }).$mount('#app');
} else {
  window.location.href = '/login.html';
}
