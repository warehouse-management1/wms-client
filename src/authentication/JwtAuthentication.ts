import { uuidv1 } from "@/domain/Identifiable";

export interface JwtAuthentication {
  companyId: uuidv1;
  roles: string[];
  sub: string;
  userId: uuidv1;
}
