import Identifiable from '@/domain/Identifiable';

export interface Warehouse extends Identifiable {
  location: string | null;
}
