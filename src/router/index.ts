import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/warehouse/:warehouseId/inventory',
    name: 'warehouseInventory',
    component: () => import(/* webpackChunkName: "warehouseInventory" */ '@/warehouse/view/WarehouseInventoryView.vue'),
  },
  {
    path: '/warehouse/:warehouseId/inventory/create',
    name: 'warehouseInventoryCreate',
    component: () =>
      import(/* webpackChunkName: "warehouseInventoryCreate" */ '@/inventory/view/InventoryDetailView.vue'),
  },
  {
    path: '/warehouse/:warehouseId/inventory/:inventoryId',
    name: 'warehouseInventoryEdit',
    component: () =>
      import(/* webpackChunkName: "warehouseInventoryCreate" */ '@/inventory/view/InventoryDetailView.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
