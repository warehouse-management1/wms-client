import { JwtAuthentication } from '@/authentication/JwtAuthentication';

export interface AjaxState {
  readonly runningRequests: number;
}

export default interface RootState {
  jwt: string;
  authentication: JwtAuthentication;
  ajaxState: AjaxState;
}
