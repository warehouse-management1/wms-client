import Vue from 'vue';
import Vuex, { Store } from 'vuex';
import jwtDecode from 'jwt-decode';
import RootState from '@/vuex-store/RootState';
import { JwtAuthentication } from '@/authentication/JwtAuthentication';

Vue.use(Vuex);

export const createStore = (jwt: string): Store<RootState> => {
  const authtication = jwtDecode(jwt) as JwtAuthentication;

  return new Store<RootState>({
    state: {
      jwt: jwt,
      authentication: authtication,
      ajaxState: {
        runningRequests: 0,
      },
    },
    mutations: {
      incrementRunningRequests: (state: RootState): RootState => {
        state.ajaxState = {
          runningRequests: state.ajaxState.runningRequests + 1,
        };

        return state;
      },
      decrementRunningRequests: (state: RootState): RootState => {
        const runningRequests = state.ajaxState.runningRequests;

        if (runningRequests > 0) {
          state.ajaxState = {
            runningRequests: state.ajaxState.runningRequests - 1,
          };
        }

        return state;
      },
    },
  });
};
