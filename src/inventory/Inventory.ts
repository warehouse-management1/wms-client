import Identifiable from '@/domain/Identifiable';

export default interface Inventory extends Identifiable {
  manufacturer: string | null;
  sku: string | null;
  name: string | null;
  picked: boolean | null;
  warehouse: Identifiable | null;
}
