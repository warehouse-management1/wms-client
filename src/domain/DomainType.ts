/**
 * Represents a simple domain type that describes what a relationship is.  Think enum.
 */
export default interface DomainType {
  readonly value: string;
  readonly description: string;
}

/**
 * Checks that an object provides the minimum of what the API will accept for a Domain Type, which is a property of value
 *
 * @param toBeDeterminedDomainType
 */
export const isDomainType = (toBeDeterminedDomainType: any): toBeDeterminedDomainType is DomainType =>
  (toBeDeterminedDomainType as DomainType).value !== undefined;
