export type uuidv1 = string;

export default interface Identifiable {
  id: uuidv1 | null;
}

export class IdentifiableConstructor implements Identifiable {
  public id: string | null;

  constructor(id: string | null = null) {
    this.id = id;
  }
}

export const idComparator = (a: Identifiable, b: Identifiable): number => (a.id ?? '').localeCompare(b.id ?? '');

/**
 * Check that an object is following the Identifiable contract
 *
 * @param toBeDeterminedIdentifiable
 */
export const isIdentifiable = (toBeDeterminedIdentifiable: any): toBeDeterminedIdentifiable is Identifiable =>
  (toBeDeterminedIdentifiable as Identifiable).id !== undefined;

/**
 * Check that an object has more than just an id property when following the Identifiable contract
 *
 * @param toBeDeterminedIdentifiable
 */
export const isNotJustIdentifiable = (toBeDeterminedIdentifiable: any): toBeDeterminedIdentifiable is Identifiable =>
  isIdentifiable(toBeDeterminedIdentifiable) &&
  Object.keys(toBeDeterminedIdentifiable).filter((key) => key.trim() !== 'id' && !key.startsWith('_')).length > 0;
