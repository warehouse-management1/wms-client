export default class RequestError {
  public readonly message: string;
  public readonly path?: string;

  constructor(message: string, path?: string) {
    this.message = message;
    this.path = path;
  }
}
