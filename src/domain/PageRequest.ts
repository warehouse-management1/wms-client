export default interface PageRequest {
  page: number;
  size: number;
  sortBy: string;
  sortDirection: string;
}

export class PageRequestConstructor implements PageRequest {
  public readonly page: number;
  public readonly size: number;
  public readonly sortBy: string;
  public readonly sortDirection: string;

  constructor(page = 1, size = 10, sortBy = 'ID', sortDirection = 'ASC') {
    this.page = page;
    this.size = size;
    this.sortBy = sortBy;
    this.sortDirection = sortDirection;
  }
}

export const defaultPageRequest = new PageRequestConstructor();
export const defaultPageRequestDescending: PageRequest = { ...defaultPageRequest, sortDirection: 'DESC' };
