import Identifiable from '@/domain/Identifiable';
import PageRequest, { PageRequestConstructor } from '@/domain/PageRequest';
import { DataOptions } from 'vuetify';

export default interface Page<T extends Identifiable> {
  elements: T[];
  requested: PageRequest;
  totalElements: number;
  totalPages: number;
  first: boolean;
  last: boolean;

  toDataOptions(): DataOptions;
}

export function isPage(toBeDeterminedPage: any): toBeDeterminedPage is Page<any> {
  return !!(toBeDeterminedPage as Page<any>).elements;
}

export class PageImpl<T extends Identifiable> implements Page<T> {
  public readonly elements: T[];
  public readonly first: boolean;
  public readonly last: boolean;
  public readonly requested: PageRequest;
  public readonly totalElements: number;
  public readonly totalPages: number;

  constructor(
    elements: T[],
    first: boolean,
    last: boolean,
    requested: PageRequest,
    totalElements: number,
    totalPages: number,
  ) {
    this.elements = elements;
    this.first = first;
    this.last = last;
    this.requested = requested;
    this.totalElements = totalElements;
    this.totalPages = totalPages;
  }

  public toDataOptions(): DataOptions {
    return {
      page: this.requested.page,
      itemsPerPage: this.requested.size,
      sortBy: [this.requested.sortBy],
      sortDesc: [false],
      groupBy: [],
      groupDesc: [],
      multiSort: false,
      mustSort: false,
    };
  }
}

export function toPageRequest(dataOptions: DataOptions): PageRequest {
  return new PageRequestConstructor(
    dataOptions.page,
    dataOptions.itemsPerPage,
    dataOptions.sortBy[0],
    dataOptions.sortDesc[0] ? 'DESC' : 'ASC',
  );
}

export const emptyPage = new PageImpl<any>([], true, true, new PageRequestConstructor(), 0, 0);

export const firstPage = <T extends Identifiable>(page: Page<T>): PageRequest => {
  const requested = page.requested;

  return {
    page: 1,
    size: requested.size,
    sortBy: requested.sortBy,
    sortDirection: requested.sortDirection,
  };
};

export const previousPage = <T extends Identifiable>(page: Page<T>): PageRequest => {
  const requested = page.requested;

  return {
    page: requested.page - 1,
    size: requested.size,
    sortBy: requested.sortBy,
    sortDirection: requested.sortDirection,
  };
};

export const nextPage = <T extends Identifiable>(page: Page<T>): PageRequest => {
  const requested = page.requested;

  return {
    page: requested.page + 1,
    size: requested.size,
    sortBy: requested.sortBy,
    sortDirection: requested.sortDirection,
  };
};

export const lastPage = <T extends Identifiable>(page: Page<T>): PageRequest => {
  const requested = page.requested;
  const totalElements = page.totalElements;
  const pageSize = requested.size;

  return {
    page: Math.ceil(totalElements / pageSize),
    size: requested.size,
    sortBy: requested.sortBy,
    sortDirection: requested.sortDirection,
  };
};

export const samePage = <T extends Identifiable>(page: Page<T>): PageRequest => {
  const requested = page.requested;
  const totalElements = page.totalElements;
  const pageSize = requested.size;
  const lastPage = Math.ceil(totalElements / pageSize);
  const requestedPage = requested.page > lastPage ? lastPage : requested.page;

  return {
    page: requestedPage,
    size: requested.size,
    sortBy: requested.sortBy,
    sortDirection: requested.sortDirection,
  };
};
