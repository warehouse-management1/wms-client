import Client from '@/common/infrastructure/Client';
import Vue from 'vue';
import { Store } from 'vuex';
import RootState from '@/vuex-store/RootState';

declare module 'vue/types/vue' {
  interface Vue {
    $http: Client;
  }
}

export default function ClientPlugin(vue: typeof Vue, options: ClientPluginOptions): void {
  Vue.prototype.$http = new Client(
    options.accessToken,
    options.store,
    () => {
      options.store.commit('incrementRunningRequests');
    },
    () => {
      options.store.commit('decrementRunningRequests');
    },
  );
}

export interface ClientPluginOptions {
  readonly accessToken: string;
  store: Store<RootState>;
}
